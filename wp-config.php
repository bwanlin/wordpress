<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'db_wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'smallville2401');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost:3306');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PD{&M*NHT;SH#7h}RIc/Ifwy)Lw{mUzat>=JQ!amou;.0R<4X%:=+pj S|t:g;*s');
define('SECURE_AUTH_KEY',  '_^+[e^0|3Ue|j+Js(!Oo;t2v(E<a[We&=(%TfSjn,z5yD911#i|$qupS[x+){)c2');
define('LOGGED_IN_KEY',    '&x+|I/q;wW1KKE-cJp02Z+lY.-?=7;)ic:X>{.T1b9|Q|/<#Z`~tjXvd6=QE|U7K');
define('NONCE_KEY',        ';!sv}jqzOz%$<!td{VBcB0V+Ev/6@+bDVYFRPg,{|UuzIJydz!Iy}_`8.y/&6XU#');
define('AUTH_SALT',        'XOgb::D_K@lKI8 )HySCE)Swt8a{eDMe:ZX6fLJw>Io)P+e}zB|L V9#bL&N81!Q');
define('SECURE_AUTH_SALT', ']OnM{3[;|*F?2[$]^HGZ@]|&2c>?DRo7p#9nr>Ze*|$s|.||`~-Fd#a-6[;jCH|W');
define('LOGGED_IN_SALT',   'PC7[d]jih|HAUSt164!!DhU]>j>A:[;6w#rZg;UDUf8#iA4yEKV8xKj}FVj7%_4*');
define('NONCE_SALT',       '{A:zd:-Wi;YM,VOp~z>)6ly]_y;M]6sO(HOQ=.Z@uCOGOTdF8g(Iq[03Eh/qtFu,');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');