<?php 
/* 
 *  Plugin Name: MON PREMIER PLUGIN WP 
 *  Description: DESCRIPTION DU PLUGIN 
 *  License: GPL … 
 *   Author NOM DE L’AUTEUR 
 *    Version: 1.0 
 *    */ 
 
function fb_add_custom_user_profile_fields( $user ) { // On ajoute les données ici 
?> 
 <h3><?php _e('MORE INFOS', 'your_textdomain'); ?></h3> 
 <table class="form-table"> 
 <tr> 
 <th> 
 <label for="facebook"><?php _e('Facebook Account', 'your_textdomain'); ?></label> 
 </th> 
 <td> 
 <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 
	 'facebook', $user->ID ) ); ?>" class="regular-text" required /><br /> 
 <span class="description"> 
 <?php _e('Facebook account', 'your_textdomain'); ?> 
 </span> 
 </td>
  <td> 
 <input type="text" name="dailymotion" id="dailymotion" value="<?php echo esc_attr( get_the_author_meta('dailymotion', $user->ID )); ?>" class="regular-text" required /><br /> 
 <span class="description"> 
 <?php _e('Video dailymotion', 'your_textdomain'); ?> 
 </span> 
 </td> 
 <td> 
    <select id="select" name="select">
      <option value="Joyeux"<?php if ( esc_attr( get_the_author_meta( 'select', $user->ID )) == 'Joyeux') {echo 'selected';} ?> >Joyeux</option> 
      <option value="Enervé"<?php if ( esc_attr( get_the_author_meta( 'select', $user->ID )) == 'Enervé') {echo 'selected';} ?>>Enervé</option>
      <option value="Indigné"<?php if ( esc_attr( get_the_author_meta( 'select', $user->ID )) == 'Indigné') {echo 'selected';} ?>>Indigné</option>
    </select>
 <span class="description"> 
 <?php _e('Humor', 'your_textdomain'); ?> 
 </span> 
 </td> 

 </tr> 
 </table> 
<?php } 
 
function fb_save_custom_user_profile_fields( $user_id ) { // On sauvegarde les données ici 
	 
	 if ( !current_user_can( 'edit_user', $user_id ) ) 
		  return FALSE; 
	  
	  update_usermeta( $user_id, 'facebook', $_POST['facebook'] );
	   update_usermeta( $user_id, 'dailymotion', $_POST['dailymotion'] ); 
	    update_usermeta( $user_id, 'select', $_POST['select'] );
} 
 
add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' ); 
 add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' ); 
  
add_action( 'personal_options_update', 'fb_save_custom_user_profile_fields' ); 
add_action( 'edit_user_profile_update', 'fb_save_custom_user_profile_fields' ); 
?>
