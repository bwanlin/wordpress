<?php get_header() ?>
<div id="content">
<?php the_post() //cf. codex the_post() ?>
	<div class="entry">
		<h2 class="page-title"><?php the_title() ?></h2>
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail();
        } else {
            if ( get_post_meta($post->ID, 'id_dailymotion', true)){
                ?><iframe frameborder="0" width="480" height="270" src="http://www.dailymotion.com/embed/video/<?php echo get_post_meta($post->ID, 'id_dailymotion', true); ?>" allowfullscreen>
                </iframe><?php
            } else {
                ?><img src="http://www.lyssal.fr/emulation/super_nintendo/la_legende_de_zelda_a_link_to_the_past/excalibur.jpg"/><?php }
        }

        ?>
		<div class="entry-content">
		<?php the_content() //cf. codex the_content() ?>
		<?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>
		</div>
	</div><!-- entry -->
<?php if ( get_post_custom_values('comments') ) comments_template() ?>
</div><!-- #content -->
<?php if ( get_post_meta($post->ID, 'desc-img', true) ) : ?>
    //Cf. Codex get_post_meta()
    <?php echo get_post_meta($post->ID, 'desc-img', true) ?>
<?php endif; ?>
<?php get_sidebar() ?>
<?php get_footer() ?>