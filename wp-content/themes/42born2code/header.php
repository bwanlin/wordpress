<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes() ?>>
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title><?php wp_title( '-', true, 'right' ); echo wp_specialchars( get_bloginfo('name'), 1 ) ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css" media="screen" />

	<!--[if lte IE 6]><script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/supersleight-min.js"></script><![endif]-->
<?php wp_enqueue_script(get_bloginfo('template_directory').'/js/jquery.js'); ?>
<?php wp_enqueue_script('superfish', get_bloginfo('template_directory').'/js/superfish.js', array('jquery'), '1.7'); ?>
<?php wp_enqueue_script(get_bloginfo('template_directory').'/js/nav.js'); ?>
<?php if (trim(get_option('ft_header_code')) <> "") { echo stripslashes(get_option('ft_header_code')); } ?>
<?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

<?php wp_head(); ?> <!-- #NE PAS SUPPRIMER cf. codex wp_head() -->
</head>
<body <?php body_class() ?>>
<div id="top">
	<div class="pads clearfix"><?php wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'sort_column' => 'menu_order', 'container_class' => 'nav' ) ); ?>
	</div>
</div>
<div id="header" style="width: 1058px; height: 300px; margin: 0 auto;">
	<div style="" class="pads clearfix">
		<a href="<?php echo get_option('home'); ?>">
			<img style="position: absolute; margin-left: 5px; margin-top: 5px;" id="site-logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>" />
		</a>
		<div id="blocsearch" style="position: absolute; margin-left:780px; margin-top: 185px;"> <?php include('searchform.php'); ?></div>
		<div class="nav-wrap">
			<div id="nav" style="margin-left: 100px; margin-top: 180px;">
			<?php wp_nav_menu( array( 'menu' => 'menuH', 'theme_location' => 'primary-menu', 'sort_column' => 'menu_order', 'container_class' => 'nav' ) ); ?>
			</div>
		</div>
        <?php if (!is_user_logged_in()){ ?>
        <span style="position: absolute; margin-left: 663px; margin-top: 1px;" ><a href="http://wp.local.42.fr:8080/?page_id=101&log=true">accès candidat</a></span>
        <span style="position: absolute; margin-left: 663px; margin-top: 25px;" >préinscris-toi</span>
            <div class="clearfix"></div>
        <form style="position: absolute; margin-left: 660px; margin-top: -170px;" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>"method="post">
            <input type="text" name="user_login" value="Username" id="user_login" class="input" />
            <input type="text" name="user_email" value="E-Mail" id="user_email" class="input"  />
            <?php do_action('register_form'); ?>
            <input style="position: absolute;" type="submit" value="Register" id="register" />
        </form>

        <span style="position: absolute; margin-top: 100px; margin-left: 50px;">
        <?php
            if ($_GET['log'] == true)
                wp_login_form(); ?></span>
        <?php } else {?>
            <a href="<?php echo wp_logout_url(); ?>" style="position: absolute; margin-left: 663px; margin-top: 1px;" title="Logout">Logout</a>
        <?php }?>
    </div>
</div><!--  #header -->

<div id="container">
	<div class="pads clearfix">